= Now is the time&hellip;
:slug: now_is_the_time
:lang: en
:date: 2021-01-17
:author: Siltaar
:experimental: yes

Meta-Press.es is approaching the end of it's NLnet support. We've done a lot of work thanks to them, and some cool features (such as automatic scheduled research) are to be released in the coming weeks.

2020 should have been a year full of public presentations for me, but everything have been canceled. I only managed to speak at an online event in July, which promised videos are still not online. So I call for a *crowd-spreading of Meta-Press.es* ! (and I hope I'll be received for https://pretalx.jdll.org/jdll2021/cfp[2021 JDLL])

So now is a good time for the small but faithful community to engage and help Meta-Press.es reaching it's target audience : everyone making press reviews !

Step up, speak about Meta-Press.es, to your family and friends, to your NGO, to local communities… Become an ambassador.

I had contacts with school documentation centers using Meta-Press.es, with researchers, with a literary blog maker, a Thomson-Reuters journalist. They all use Meta-Press.es and plan to use it more. This is always a great encouragement.

We plan to reach 300 sources this semester, but special efforts (setting field, full documentation…) were made to allow web developers to easily *integrate new sources*, and around 50 sources were already contributed. You can help us discovering and integrating more source.

A last but new way to help the project is to register for a small *Patreon monthly support*. Liberapay is still great, libre and available, but I opened a Patreon page to circumvent its current lack of recurring donations. The future of the project is in your https://patreon.com/metapress[clicks] now !
