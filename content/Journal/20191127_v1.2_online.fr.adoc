= Version 1.1 et 1.2 de Meta-Press.es : plus de 100 sources !
:slug: release_of_meta-press.es_v1.2
:lang: fr
:date: 2019-11-27
:author: Siltaar

La version 1.1 de Meta-Press.es est sortie dans la semaine de la 1.0, pour
répondre rapidement aux premiers retours constructifs que j'ai reçus.

Et puis, lors de mon voyage jusqu'à Turin (pour le symposium DECODE) j'ai pu
m'entretenir avec Benjamin Bayart qui m'a convaincu d'accepter de prendre des
flux RSS en entrée (pour profiter de la stabilité du format dans le temps) et
pour s'ouvrir aux radios (et leurs podcasts).

Pour ce faire, j'ai dû ajouter la possibilité d'utiliser des chemins _XPath_ à la
place des sélecteur CSS.

J'en ai profité pour simplifier la façon dont les sources sont contribuées, en
mettant en place un format plus simple pour la description des sources. Elles
sont désormais décrites en JSON, une syntaxe simple et bien documentée, plus
besoin de programmer en JavaScript. Cela a également eu pour effet d'assainir
les entrées utilisateurs (qui ne sont plus que du JSON donc) et s'évitant
l'utilisation d' `eval()`. Il n'y a désormais plus de logique embarquée dans les
données, et ce fichier JSON de données pures pourra être réutilisé par d'autres
programmes au besoin (tout comme
https://framagit.org/Siltaar/month_nb[month_nb]).

Elisa, la graphiste de Meta-Press.es a produit une nouvelle icône, une animation
pour faire patienter pendant les recherches et un visuel pour remplir la page
vide d'une recherche infructueuse.

Pour finir, j'ai ajouté de *nouvelles sources et il y en a désormais plus de
100, réparties sur 38 pays et en 21 langues*. Un effort particulier a été fait
sur les sources en français, avec maintenant 24 sources.  *14 sources
correspondent à des moteurs de recherche de publications scientifiques* (merci
https://framagit.org/remyd1[Remyd1]). La proportion de sources fournissant du
RSS est de 30% de la collection. C'est plus rapide à intégrer car la structure
est déjà connue et que le format de date est standard, mais on perds
l'information du nombre total de résultats connus, puisqu'on a plus alors que
les 20 résultats embarqués dans le flux RSS fourni.

Mozilla continue de comptabiliser *environ 350 utilisateurs quotidien* depuis un mois.

Voici la liste des modifications apportées par ces nouvelles versions.


== version 1.2 :

- nouveau favicon
- illustration quand il n'y a pas de résultats
- import de flux RSS (et ATOM)
- nouveau format simplifié de description des sources
- lecture des résultats de recherches en RSS (même avec images)
- support des définition de source par héritage d'une source existante (12
  langues pour Euronews…)
- affichage de statistiques sur les sources disponibles dans le pieds de page
- coloration syntaxique dans le champs de définition d'une nouvelle source
  (dans le panneau de paramètres)
- mécanisme empêchant les sélections vides avec le mécanisme de tags
- réparation du mode sélection, toutes les pages sont maintenant affublées de
  cases à cocher d'un coup
- raccourcissement automatique des trop longues listes de co-auteur d'article
- affichage des contenus coupés en entier en infobulles
- export des sélections d'article en RSS (ou ATOM)
- bouton pour arrêter une recherche en court (recharge la page, car
  l'annulation de _promises_ n'est pas encore définie dans la norme)
- rechargement des gros titres à chaque changement de tag


== version 1.1 :

- affichage de l'état d'une recherche infructueuse ("No results")
- affichage d'une animation d'attente pendant les recherches
- reformulations de plusieurs point et ajout d'aide en infobulles (merci
  https://framagit.org/goofy[Goofy])
- chargement des gros titres pour les journaux sélectionnés seulement
