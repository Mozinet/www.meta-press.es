= Release of Meta-Press.es v1.1 and v1.2 : more than 100 sources !
:slug: release_of_meta-press.es_v1.2
:lang: en
:date: 2019-11-27
:author: Siltaar

The v1.1 of Meta-Press.es has quickly been released one week after the v1.0 in
order to address the firsts kind feedbacks I received.

Then, on my way to Turin (and the DECODE Symposium) I met with Benjamin Bayart
and he convinced me to accept RSS format as an input format, in order to get
stable parsing of sources and to open-up to radios (and their podcasts) in
addition to newspapers.

To do this I needed to adopt _XPath_ in addition to CSS selectors.

I also decided to simplify the way sources are contributed by people, setting a
new simpler format to describe them. It's now done only in JSON which is a
simple syntax well documented, no more JavaScript programming required. It also
provided a user input sanitizing process, avoiding the usage of `eval()`.  Now
there is no logic embeded with the data and it might be reused by other
programs (just like https://framagit.org/Siltaar/month_nb[month_nb]).

Elisa, the designer of the artwork of Meta-Press.es also produced a new favicon
an animation to wait during the search and an image to illustrate the empty
screen of a no-results search.

To finish, I added new sources and there is now *more than 100 sources, for 38
countries and in 21 languages*. A special effort have been made for French
sources, and there is now 24 of them. *14 sources are for scientifique press*
(thx to https://framagit.org/remyd1[Remyd1].  30% of the collection is RSS
based. It's faster to add as the data structure is already known and the date
format is standard, but we loose the exact number of existing results for those
sources as we just have access to the 20 results embeded in the provided RSS
file.

Mozilla reports a stable *350+ daily user count* since a month.

Here is the list of modifications.


== version 1.2 :

- new favicon
- illustration when there is no results
- import RSS (and ATOM) flux
- new lighter format to add sources, 100% JSON
- support for live RSS import of results (even with images)
- support for derivative sources (12 languages of Euronews…)
- show stats of loaded sources in footer :
- syntax highlighting of sources in setting panel, via codemirror
- prevent empty selection of sources
- fix toogle selection mode : now toggles even not displayed results
- ellipsis for long list of authors, long headlines, too long excerpts
- full contents still here but in hover tooltips
- export source filters when exporting RSS (and ATOM) flux
- fix URL encoding in RSS and ATOM exports (also remove CDATA sections)
- cancel button to reload page during too long queries (as canceling promises
  is not yet supported by the API)
- reload headlines at each tag selection


== version 1.1 :

- display "No results" when the query returns no results
- waiting anim rendering fixed
- rephrase various points, add tooltips to document features (thx
  https://framagit.org/goofy[Goofy])
- load headlines only for selected newspapers
