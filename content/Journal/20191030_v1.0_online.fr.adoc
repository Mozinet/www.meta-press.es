= Sortie de Meta-Press.es v1.0 et feuille de route 2020
:slug: release_of_meta-press.es_v1.0
:lang: fr
:date: 2019-10-30
:author: Siltaar

Mozilla vient tout juste d'approuver Meta-Press.es v1.0 dans addons.mozilla.org. Il est désormais possible d'*installer cette extension de Firefox* en https://addons.mozilla.org/en-US/firefox/addon/meta-press-es/[quelques clics].

Cette version apporte les améliorations suivantes à la link:/journal/2017/motivations.html[preuve de concept] précédemment publiée :

- Le support de la sélection des journaux dans lesquels chercher, via un système de filtres, sauvegardés dans le stockage des extensions de Firefox entre deux utilisations ;
- Le support de l'exportation (et importation) des résultats (ou d'une sélection de résultats) via un fichier .JSON ;
- *Le support de l'export des résultats (ou d'une sélection) en RSS* ou ATOM ;
- Le support de 50 sources, dont 10% ont été contribuées ;
- Le support de la définition de nouvelles sources depuis le panneau des préférences de l'extension.

Et plein de petites améliorations techniques : comme le pré-chargement DNS des journaux footnote:[Bien que je cherche encore un moyen simple de vérifier que ça fonctionne, vu que l'onglet Réseau des outils développeurs de Firefox ne liste pas les requêtes DNS.], l'apparition d'une animation pour patienter avant l'arrivée des premiers résultats footnote:[J'ai hacké un .gif avec the Gimp pour le ralentir, et il en ressort mal rendu, j'aurais également besoin d'un coup de main à ce sujet…], des corrections de CSS, le remplacement du mécanisme de sous-recherche dans les résultats par un plus efficace.

J'ai donc couvert les points : 2. ; 3. ; 4. ; ½ 5 ; ½ 6 ainsi que les bugs listés dans l'intro de ma link:/journal/2017/roadmap_2018.html[précédente feuille de route], et les promesses du texte d'accroche de la page d'accueil sont désormais tenues.

*Pour la suite*, nous allons pouvoir nous concentrer sur link:/journal/2019/first_contributed_newspapers.html[la base de journaux], et en définir : 200, 2000, 20 000 ? Jusqu'où pouvons nous aller avec le système actuel, en gardant une bonne réactivité du système de filtre par étiquettes ?

Le système de vérification routinière du fonctionnement des journaux définis (le point 1. de ma précédente feuille de route) serait déjà bien utile avec 50 sources footnote:[Vu que 7 sont déjà rapportées comme "cassées".] et il va devenir indispensable.

Enfin, si une large audience se penche sur ce travail, il faudra songer à internationaliser l'extension (i18n), même s'il est déjà possible de chercher dans quelques journaux en français (en plus des anglais) et de définir des journaux dans presque toutes les langues en utilisant link:/journal/2019/month_nb-released.html[month_nb] pour convertir les dates.

image::/images/20191029_meta-press_europe_selection-mode_crop_640.png[link="/images/20191029_meta-press_europe_selection-mode_crop.png", title="20191028 capture du mode de sélection de résultat, cliquer pour agrandir."]
