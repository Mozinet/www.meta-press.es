= Convertir des dates avec month_nb
:slug: month_nb-released
:lang: fr
:date: 2019-09-19
:author: Siltaar

À la fin de l'année 2013, je travaillais sur un prototype de ce qui allait
devenir Meta-Press.es quand je suis tombé sur une difficulté inattendue :
l'analyse des dates internationales (avec JavaScript).

Bien que JavaScript sache utiliser plusieurs formats de date anglaises pour
créer un objet de type Date (via `news Date()`) dès que vous avez une date en
français y'a plus personne et il faut soit convertir les dates en anglais (ou
au format ISO), soit installer une grosse footnote:[DateJS utilise en moyenne 50
fois plus de lignes de code par langues que month_nb, et Moment.js 23x plus
(3,82 lignes par langue en moyenne pour month_nb, 190 pour DateJS et 88 pour
Moment.js).] bibliothèque JavaScript basant son analyse des dates sur des
traductions contribuées par des humains
(https://github.com/datejs/Datejs[DateJS], https://momentjs.com/[Moment.js]).

Sur le moment je me suis contenté d'empiler 12 expressions rationnelles, ça
suffisait bien pour les numéros de mois en français (et alors la conversion
vers le format ISO est aisé). Mais en y réfléchissant bien les noms de mois en
anglais et en français se ressemblent beaucoup et il ne fut pas compliqué de
modifier les expressions rationnelles pour fonctionner aussi avec les noms de
mois en anglais, et même pour les 20 langues latines listées par la
https://library.princeton.edu/departments/tsd/katmandu/reference/months.html[documentation
de cataloguage de l'univeristé de Princeton].

Comme les expressions rationnelles s'allongeaient j'ai ensuite décidé de les
structurer en index pour améliorer en moyenne le nombre de tests par requête.
Par exemple : « janvier », « juin » et « juillet » commencent tous par un « j »
et donc si le nom de mois ne commence par cette lettre on peut éviter trois
tests en en ajoutant un devant. On peut également grouper « mars » et « mai »,
« avril » et « août »…

La semaine suivante j'ai décidé de continuer mon voyage autour du monde en
prenant une carte plus grande :
l'https://en.wiktionary.org/wiki/Appendix:Months_of_the_Year[appendice des noms
de mois du Wiktionnaire]. J'ai ainsi pu coder la reconnaissance des mois de 50
langues en ajoutant une première barrière d'expressions rationnelles pour
détecter l'alphabet utilisé pour le nom du mois à traiter afin de distinguer
l'alphabet cyrillique de l'alphabet latin, ou encore les caractères asiatiques.

Et puis j'ai mis le résultat quelque part en ligne et le projet a dormi pendant
6 ans attendant que Meta-Press.es voit le jour. Il semble que personne n'a
trouvé le fichier, ou n'a décidé de l'utiliser, vu que personne ne m'a contacté
à ce sujet. Mais bon, j'voulais pas trop en faire de pub non plus vu la piètre
qualité de l'architecture logicielle…

Navigant encore dans les souvenirs de la
https://fr.eloquentjavascript.net/contributors.html[traduction en français] du
livre web Eloquant JavaScript de Marijn Haverbeke, je savais qu'il me faudrait
séparer le code des données pour améliorer le programme.


> Sous la surface de la machine, le programme évolue. Sans effort, il prend de
l’ampleur et se contracte. Avec beaucoup d’harmonie, les électrons se
dispersent et se regroupent. Les formes sur le moniteur ne sont que l’écume
de la vague.
>
> Quand les créateurs ont construit la machine, ils y ont mis un processeur et
de la mémoire. À partir de là surgissent les deux points de vue sur le
programme.
>
> Du côté du processeur, l’élément actif est appelé Contrôle. Du côté de la
mémoire, l’élément passif est appelé Données.

-- https://fr.eloquentjavascript.net/chapter6.html[Les deux points de vue], Le livre de la programmation, EloquentJavascript.net, Marijn Haverbeke

Ici, les expressions rationnelles constituaient les données et mon vulgaire
tas de `if` constituait le code. Malheureusement ce problème n'était plus
alors une priorité pour moi et l'idée de réorganiser le code me trotta dans
la tête tout ce temps.

`month_nb` attendit donc l'occasion suivante de se rendre utile, ce qui advint
avec la reprise du développement de Meta-Press.es. J'ai donc récemment pris le
temps de démêler le code qui incluait des morceaux de logique un peu partout,
par exemple pour ventiler les cas de mois cyrilliques suivant l'organisation
latine ou non, ou encore pour convertir directement les noms de mois chinois
(puisqu'ils sont nommés d'après leur numéro dans cette langue).

J'ai finalement obtenu une structure arborescente de données JSON, dans
laquelle chaque clé est une expression rationnelle, débouchant soit sur un
sous-objet (une branche de l'arbre), soit sur un nombre (une feuille). Le code
est devenu un simple parcours d'arbre, en profondeur d'abord. 75% du précédent
fichier a été converti en une structure de donnée réutilisable.

Et `month_nb` ressemble enfin à ce que je voulais en faire. Il supporte 69
langues, et n'a pas besoin qu'on lui précise la langue du mois présenter pour
retrouver son numéro.

La prochaine étape sera de mettre à jour ma copie de l'appendice du
Wiktionnaire listant les noms de mois dans toutes les langues, et de mettre à
jour la structure de donnée en conséquences. 14 langues ont été ajoutées au
document entre temps.

Le programme mériterait probablement d'être empaqueté pour NodeJS, mais peut être
que quelqu'un pourra contribuer cette partie ?

En attendant, il est désormais possible d'utiliser la fonction `month_nb` pour
https://framagit.org/Siltaar/meta-press-ext#user-content-how-to-add-a-newspaper-to-the-search-engine[ajouter
un nouveau journal à l'index de Meta-Press.es] !
