= Release of Meta-Press.es v1.3
:slug: release_of_meta-press.es_v1.3
:lang: en
:date: 2020-01-13
:author: Siltaar

The versions 1.3 have been released it introduces new tags for sources and an
improved tag-based source selection mechanism.

It's now possible to select sources based on their type (newspaper, radio,
TV…), the type for their results (articles, videos, podcasts), or even just to
cherry pick the sources you want to search in.

Also, tag filtering is cumulative inside a criteria and intersecting between
criterion, except for the 1st tag field which is working by intersection also
between the selected tags. This exception have been made to allow users to do
efficient and-logic queries in HTTPS sources only out of the box.

To finish, the current number of sources for the next search is now displayed
and updated after each tag selection.

Also, since we're back from end-year celebrations Mozilla indicates 800+ users
by days (since last 7 days).
