= Version 1.6 : Presse de r&eacute;f&eacute;rence, accessibilit&eacute; et Android
:slug: v1.6_newspapers_of_record_accessibility_and_android_compatibility
:lang: fr
:date: 2020-11-25
:author: Siltaar
:experimental: yes


Cela vous surprendra peut être, mais cette version apporte d'avantage de
sources et de nouvelles fonctionnalités.

Concernant les sources, les
https://en.wikipedia.org/wiki/Newspaper_of_record["Journaux de référence"] (ces
journaux prestigieux et internationaux footnote:[listés sur la page
correspondante anglaise de Wikipedia]) ont tous été intégrés à la collection,
ou à la liste des sources incompatibles (disponible
https://framagit.org/Siltaar/meta-press-ext/-/wikis/Incompatible-sources[ici]).

En comptant les autres sources françaises ajoutées récemment, nous arrivons à
un total légèrement supérieur à **240 sources !**

Il y a désormais 20 sources pour les résultats de type "photo" (et 20 autres
pour les "vidéos"). De plus des images ont été ajoutées en illustration de
résultats de type "texte", portant le nombre de sources capables de retourner
des images à 40.

Le traitement des images dans les résultats a été simplifié, par l'ajout de
champs dédiés dans la description d'une source (la documentation correspondante
arrivera bientôt).

Le support d'**Android** via
https://f-droid.org/en/packages/org.gnu.icecat/[Icecat Mobile] 68.4 (ou Fennec
68.12) de https://f-droid.org[F-Droid.org] est désormais complètement
opérationnel, depuis le changement du sélecteur multiple d'étiquettes (utilisé
pour le filtrage des sources dans lesquelles chercher) de
https://github.com/Mobius1/Selectr/[Selectr] à
https://github.com/jshjohnson/Choices[Choices.js] par Christopher et l'abandon
des appels à `String.replaceAll()` https://caniuse.com/?search=replaceAll[mal
supportés]. Nous avons également choisi de rediriger les messages de la console
du navigateur vers une fenêtre `alert()`, à la demande, pour faciliter les
opérations.

Après quelques dysfonctionnements avec le
https://www.torproject.org/download/[Tor Browser], la situation est revenue à
la normale. J'ai constaté des problèmes avec la version 9.5.1 et quand je me
suis penché sur la version 10.0 pour y travailler, tout fonctionnait.

Le dernier gros morceau de cette mise en ligne concerne l'**accessibilité**.
En effet l' https://accessibility.nl[Accessibility Foundation] financée par le
https://nlnet.nl[NGI0 project] a conduit un audit sur Meta-Press.es. 19 points
ont été contrôlés et Meta-Press.es a été évaluée avec un niveau : WCAG 2.1 "AA".
À la lecture de ce rapport j'étais donc plutôt content de développer du bon
logiciel, avec 6 points directement approuvés.

Toutefois, après cette bouffée d'auto-satisfaction, j'ai commencé à lire les
descriptions personnalisées des problèmes rencontrés dans les 13 autres points…
Je me suis alors senti tiraillé entre les critiques formulées contre mon
travail et l'évidence des descriptions concrètes des problèmes dénoncés. Des
solutions étaient bien souvent suggérées. Je me suis tout d'abord rebellé
contre cette offense à ma liberté d'expression artistique, et puis, j'ai pris
une grande respiration et j'ai commencé à tester les règles de sélection de
contraste dans le choix des couleurs.

J'ai réussi à valider une palette de couleur acceptable à la fois pour la
_WebExtension_
https://addons.mozilla.org/en-US/firefox/addon/wcag-contrast-checker/[WCAG
addon Contrast checker by Rumoroso] et par ma sensibilité artistique, et ça m'a
semblé représenter tout de même une amélioration.

À partir de là, j'ai étudié chaque point en détail : ajouté de la titraille,
défini clairement la raison d'être des liens, vérifié la présence des attributs
`alt` sur les images, https://validator.w3.org/[validé le code HTML] (et donc
ne pas utiliser de lien sans `href`, donc utiliser des boutons à la place…),
gardé un ordre logique pour la focalisation des éléments (donc ne pas changer
celui par défaut, mais pourquoi pas ajouter un lien invisible en haut de page
pour renvoyer directement au champs de saisie des requêtes ?) pour finalement
en arriver au piège à clavier (le _keyboard trap_).

L'accessibilité n'est pas une science occulte, il s'agit surtout de se mettre à
la place de ceux qui naviguent sur le web via une tablette Braille ou un
lecteur d'écran. Ces périphériques spécifiques sont d'après l'__Accessibility
Foundation__, moins tolérants qu'un navigateur dernier cri au HTML non valide
pour commencer. Mais ensuite, s'il n'y a plus de souris, est-ce qu'il est
toujours possible de découvrir et utiliser votre interface web ?

Dans ce contexte, on commence à s'appuyer sérieusement sur la touche kbd:[tab]
pour interagir avec les pages. Le *_keyboard trap_*, c'est quand un _widget_
élaboré se met à interpréter les signaux de la touche kbd:[tab] pour en faire
quelque chose qui ne rend pas le focus au navigateur et donc au _widget_
suivant de la page. Avec un _widget_ élaboré comme ça, une fois le focus
attrapé, on reste bloqué.

C'était le cas de nos instances de https://github.com/codemirror/[CodeMirror]
dans la page de Réglages. Ce _widget_ permet d'éditer du JSON avec coloration
syntaxique pour aider à définir de nouvelles sources, mais il emprisonnait le
focus.

Les personnes de l'__Accessibility Foundation__ ont toujours répondu rapidement
et efficacement à mes questions, mais concernant ce cas précis, la solution
était une fonctionnalité pas vraiment documentée, mentionnée par l'auteur du
logiciel dans un commentaire du fil de discussion d'un
https://github.com/codemirror/CodeMirror/issues/5444#issuecomment-395307632[rapport
de bogue] sur le site de développement.

En conclusion, l'apparence de la _WebExtension_ Meta-Press.es a beaucoup évolué
au cours de ce travail sur l'accessibilité, et il faut bien admettre qui ça l'a
rendue meilleure, y compris pour la majorité des utilisateurs.

J'espère que vous aller apprécier cette nouvelle version de Meta-Press.es et
que vous aurez envie d'en parler autour de vous pour aider les journalistes et
les associations qui en ont besoin à mettre la main dessus.
