= Funds from the NLnet Foundation
:slug: funds-from-the-nlnet-foundation
:lang: en
:date: 2020-03-25
:author: Siltaar

With the v1.0 online and the good stats of the launch, I applied again for
funding, where I previously got at least an answer :

- Mozilla via the MOSS
- the NLnet foundation
- the Open Society Institute (OSI)
- the OpenTech Fund (OTF)

And the NLnet answered positively !

https://nlnet.nl[NLnet] is part of the NGI Zero consortium of European
associations dealing with the complexity of the European Union’s
https://ec.europa.eu/programmes/horizon2020/[Horizon 2020] research and
innovation programme, and gathering a number of complementary skills around
software development to boost grantees with more than just money.

Their grant process is straight and efficient. It counts few steps, but those
spread across 4 months. First, they really study applications on their side
and come back with pertinent technical questions for you if your project fits
in the funded areas.

If your project is working and needs a reachable improvement, and if it
presents enough interest for the internet to take the seat of another applying
project, your application is presented to the European Commission for
validation.

Then NLnet and you establish a Memorandum of Understanding, a document that
will describe the steps of your intended work on the project and the
associated part of the grant. Once a step is reached, you can claim your grant
portion. No need for more paper work, it's charitable donation.

Here are the steps of the 2019-12-019 Memorandum of Understanding of Meta-Press.es :

- *Search automation :*
** automation of searches (at least on daily basis) https://framagit.org/Siltaar/meta-press-ext/-/issues/31[#31]
** procedure to test the sources and discover which stopped working
- *UI improvement :*
** translatable UI (and French translation) https://framagit.org/Siltaar/meta-press-ext/-/issues/30[#30]
** user preferences : for https://addons.mozilla.org/fr/firefox/addon/meta-press-es/reviews/1482250/?src=search[dark background], headline loading, number of results by sources https://framagit.org/Siltaar/meta-press-ext/-/issues/17[#17], https://framagit.org/Siltaar/meta-press-ext/-/issues/39[live-search-reload] at filter change or not…
** direct links to : report feedback https://framagit.org/Siltaar/meta-press-ext/-/issues/37[#37], create new source, reset filters https://framagit.org/Siltaar/meta-press-ext/-/issues/38[#38]
- *Browser integration :*
** ask for fewer permissions (as per https://framagit.org/Siltaar/meta-press-ext/-/issues/33[#33])
** notifications when results are available (after long search) https://framagit.org/Siltaar/meta-press-ext/-/issues/20[#20]
- *More sources :*
** double the current number of sources (120 -> 240)
** including all the https://en.wikipedia.org/wiki/Newspapers_of_record[Newspapers of Record] (English page)
- *Audits :*
** accessibility audit
** security audit

The grant is fixed at 20k€.

The https://www.wauland.de/en/[Wau Holland Foundation] kindly granted 1k€ again
this year.

*My main objective is to go from the current working state to a stable state,
easy to keep working.*

Meta-Press.es has been downloaded 2000 times during the 3 first months and is
still at 800 users by day as per Mozilla statistics.
