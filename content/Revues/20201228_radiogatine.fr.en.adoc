= [Radio G&acirc;tine] Meta-Press.es alternative solution to Google News
:slug: 20201228_radiogatine.fr
:lang: en
:date: 2020-12-28
:author: Siltaar

image::/images/20201214_radiogatine.fr_5fe1c6e31082d4.72030039.jpg[title="Simon D. in his office © 2020 Samuel Pacault, Radio Gâtine"]

And it's at Pougne-Hérisson that happens. Hence, Simon Descarpentries is a web developper for Acoeuro, a free software compagny which he is one of the associates. Since 2013, Simon worked on the creation of search engine dedicated to the press. Now besided by Christopher, it's apprentice, Simon propose this solution, militant and alternative to Google News. Called Meta-Press.es, this search engine for the press was officially launched late last year. Met at his office, in the heart of the Gâtine, this 36 y.o. entrepreneur presents us this project came true. Audio portrait.

http://radiogatine.fr/news/meta-presse-la-solution-alternative-a-google-832[*Meta-Press.es la solution alternative à Google Actualités*] (_fr_)

Note that Meta-Press.es, the press search engine dedicated to press, is accessible via an addon for the web browser Mozilla Firefox. It works without ad and need your support to continue. To learn more about it, one address : https://www.meta-press.es[www.meta-press.es]
