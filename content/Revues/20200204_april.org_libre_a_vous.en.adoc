= [April.org] Libre &agrave; vous ! radio diffusion to understand and act with April
:slug: 20200204_april_org_libre_a_vous
:lang: en
:date: 2020-02-04
:author: Siltaar

French only radio content…

https://april.org/libre-a-vous-diffusee-mardi-4-fevrier-2020-sur-radio-cause-commune[_#52 - Éducation : logiciel libre au collège – *Meta-Press.es* – Raisons d’écrire des logiciels libres « Libre à vous ! » diffusée mardi 4 février 2020 sur radio Cause Commune_]

_Présentation de Meta-Press.es, une extension pour Firefox qui permet de faire de la veille sur la presse en ligne_

_https://www.april.org/libre-a-vous-radio-cause-commune-transcription-de-l-emission-du-4-fevrier-2020[Transcription]_
