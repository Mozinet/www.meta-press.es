= [TheInternetOfThings.eu] Meta-Press.es au symposium DECODE &agrave; Turin
:slug: 20191113_theinternetofthings.eu
:lang: fr
:date: 2019-11-13
:author: Siltaar

https://www.theinternetofthings.eu/next-ngi-salon-workshop-rotterdam-during-thingscon-it-co-located-i-only-have-limited-number-free[*Au symposium DECODE à Turin il y avait de très bons projets, jetez-y un œil !*]

Simon Descarpentries

Meta-Press.es est un moteur de recherche décentralisé pour la presse que vous pouvez installer comme une extension de votre navigateur web Firefox. Il vous permet de faire des recherches dans la presse sans intermédiaire entre les journaux et vous, pour découvrir des millions de résultats en quelques secondes. Vous pouvez ensuite explorer les derniers résultats de chaque source, les filtrer, les sélectionner et les exporter.

Cela permet par exemple à des associations de bâtir leur revue de presse en quelques clics, en sélectionnant puis exportant les résultats qui les mentionnent sous la forme d'un flux RSS.

C'est un projet en logiciel libre et déjà 10% des sources dans lesquelles il est possible de chercher ont été contribués par des utilisateurs.

La version 1.0 est en ligne depuis seulement 2 semaines (après 2 ans de développement) et Mozilla comptabilise déjà 300 utilisateurs quotidiens.

Plus d'information sur le site officiel : https://www.meta-press.es
