= A propos
:slug: a_propos
:lang: fr
:experimental: yes
:toc:
:toc-title: Table des matières


== De quoi s'agit-il ?

Meta-Press.es est une extension de Firefox en logiciel libre, de meta-moteur de
recherche pour la presse en ligne.

Il fonctionne aussi très bien avec le
https://www.torproject.org/download/[Tor Browser] (qui a été conçu entre autre
pour lire la presse de manière plus confidentielle qu'avec un navigateur web
ordinaire).

Il fonctionne également sur Android via
https://f-droid.org/en/packages/org.gnu.icecat/[IceCat Mobile] (ou Fennec),
installables depuis la logithèque libre https://f-droid.org[F-Droid.org].  La
version officielle de Firefox pour Android
https://extensionworkshop.com/documentation/develop/differences-between-desktop-and-android-extensions/[ne supporte pour l'instant pas] les permissions optionnelles.

Vous pouvez l'installer depuis le 1er lien de la boîte « Ressources ».


== Comment ça marche ?

L'extension interroge chaque source (renseignée et sélectionnée) pour vous.

Vous obtenez alors le nombre total de résultats que les sources annoncent avoir
sur le sujet, ainsi que les derniers résultats publiés par chaque source.

À chaque requête, l'interface web des sources est parcourue et vous économisez
ainsi le temps que les développeurs ont consacré à automatiser le processus :-)


== Qui s'en occupe ?

Meta-Press.es est développé par +++<a rel="me" href="https://mamot.fr/@Siltaer">Simon Descarpentries</a>+++, associé d'https://acoeuro.com[Acoeuro.com].

image::/images/avatarCV2012.jpg[title="Crédit photo : Elisa de Castro Guerra, 2012"]

Vous pouvez m'aider à consacrer plus de temps à ce projet en soutenant
Meta-Press.es :

- via https://patreon.com/metapress[Patreon]
- https://liberapay.com/Meta-Press.es[Liberapay]
- Bitcoins : bc1qpkj7cu33e5y4jxjuml3e799jv6kxra729c4u03c9vyk85cz6379swqdzs3
- Ethereums : 0x89f5f205141fc8eE6540e96BF2DCec20190c584

La liste des glorieux contributeurs est maintenue https://framagit.org/Siltaar/meta-press-ext#user-content-contributors[ici].

+++<center><h2>***</h2></center>+++

Ce site est réalisé avec https://blog.getpelican.com[Pelican].

Son code source est disponible sur https://framagit.org/Siltaar/www.meta-press.es[Framagit].

Le logo a été réalisé par http://www.yemanjalisa.net/[Elisa de Castro Guerra].
