= Principes
:slug: principles
:lang: fr
:experimental: yes
:toc:
:toc-title: Index

Version du : 2018-11-09

== Du projet

Le projet Meta-Press.es suit les principes du https://www.mozilla.org/fr/about/manifesto/[Manifest Mozilla].


01. Internet fait partie intégrante de la vie moderne — il s’agit d’un composant clé dans l’enseignement, la communication, la collaboration, les affaires, le divertissement et la société en général.
02. Internet est une ressource publique mondiale qui doit demeurer ouverte et accessible.
03. Internet doit enrichir la vie de tout le monde.
04. La vie privée et la sécurité des personnes sur Internet sont fondamentales et ne doivent pas être facultatives.
05. Chacun doit pouvoir modeler Internet et l’usage qu’il en fait.
06. La réalité d’Internet en tant que ressource publique dépend de l’interopérabilité (des protocoles, des formats de données, du contenu), de l’innovation et d’une participation décentralisée mondiale.
07. Les logiciels libres et open source favorisent le développement d’Internet comme ressource publique.
08. Des processus transparents et communautaires favorisent la participation, la responsabilité et la confiance.
09. L’investissement commercial dans le développement d’Internet apporte de nombreux bénéfices ; un équilibre entre les bénéfices commerciaux et l’intérêt public est crucial.
10. Étendre et diffuser les bénéfices d’Internet pour tous, voilà un objectif important qui mérite qu’on s’y implique en y consacrant du temps et de l’énergie.


== Des contributeurs

Inspiré de la https://framacolibri.org/guidelines[charte Framacolibri].


=== Un espace civilisé pour que les personnes contribuent ensemble

Comportez vous avec les autres contributeurs comme vous le feriez dans un parc public. Nous sommes ici aussi une communauté partageant des ressources, un lieu pour partager des compétences, des savoirs, des points d'intérêts, au travers de commits et de dialogues.

Ceci est surtout un rappel de bon sens. Ces principes visent à garder le lieu propre et attrayant.

=== Soyez agréables, même lorsque vous êtes en désaccord

Vous aurez peut être à répondre à un sujet en exprimant votre désaccord. Souvenez vous de critiquer les idées, et pas leur porteurs. Évitez :

* les propos désobligeants,
* grossiers ou insultants,
* de vous attacher à la forme d'un message plutôt qu'à son fond,
* les réponses du tac-au-tac,
* d'attaquer des épouvantails,

Profitons de la diversité de nos parcours pour apprendre les un·e·s des autres. Creuser les discussions afin de comprendre la source de nos désaccords peut être précieux. Par contre, argumenter jusqu'à avoir le dernier mot par K.O. nous fait surtout perdre collectivement du temps et de la force.on.

=== Chacun compte

Aidez nous à influencer positivement le futur de cette communauté, en choisissant de participer aux débats qui feront de Meta-Press.es un projet intéressant, et d'éviter les autres.

Laissons donc le parc en meilleur état qu'on l'aura trouvé.

=== Soyez toujours courtois

Rien ne sabote un projet dynamique autant que l'impolitesse :

* Soyez courtois, ne postez rien qu'une personne raisonnable considérerait comme offensant, abusif ou haineux ;
* Respectez chacun, ne harcelez personne, ne portez pas grief, ne chosifiez pas vos interlocuteurs, n'exposez pas leur vie privée ;
* Gardez les espaces de communication propres, n'y postez pas de SPAM, ni ne les vandalisez.

Ce ne sont pas des définitions précises et juridiques, tâchez d'éviter ce qui pourrait encore ressembler à ce qui est listé ici. En cas de doute, demandez vous comment vous vous sentiriez si votre contribution était publiée en Une de l'un des journaux indexés par le projet…

Ce projet est public, et indexé par les moteurs de recherche. Gardez le niveau de langue, les liens et les images adaptés à une large audience.

=== Soyons organisés

Faîtes l'effort de poster au bon endroit, pour que nous puissions passer plus de temps à coder et débattre qu'à ranger.

* Ne rapportez un problème qu'après avoir vérifié qu'un fil de discussion traitant du même sujet n'existe pas déjà ;
* Ne postez pas la même information à plusieurs endroits à la fois ;
* Ne postez pas de réponse sans contenu probant;
* Ne changez pas le sujet d'une discussion en court de route.

== Des journaux indexés

Meta-Press.es n'affiche que les articles publiquement disponibles des journaux indexés. Le contenu des articles reste diffusé par les site web respectifs de ces journaux.

Meta-Press.es est un outil neutre, livré avec une liste par défaut de journaux indexés. Chaque utilisateur sera libre de charger d'autres listes de journaux, de faire ses propres indexations et de proposer ses listes aux autres (indexer un journal ne demande que deux URL et 5 sélecteurs CSS, rangés dans un object JSON).

Les sources de Meta-Press.es doivent publier des informations factuelles, vérifiées et vérifiables.

=== Classification
Les sources de Meta-Press.es sont classées de différentes manières, n'hésitez pas à proposer d'autres critères.

==== Classification thématique

Les thèmes actuels sont :

* régional / national / international
* financier / politique / ecologie / media / technologie / documentaire
* scientifique

==== Classifications techniques

Des filtres sur ces critères sont accessibles aux utilisateurs :

* Journaux accessibles en HTTPS ;
* Journaux n'indexant pas eux-mêmes leur contenu ;
* Journaux dont les résultats se chargent en moins de 2s ;
* Journaux permettant une liaison logique en « et » entre les termes de recherche ;
* Nature : journal, magazine, presse scientifique, radio…

==== Classifications culturelles

* Langue
* Pays
